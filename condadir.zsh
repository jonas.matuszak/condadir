#! /bin/zsh

# LICENSE
# COPYRIGHT Jonas Matuszak 2023 <jonas.matuszak@kit.edu>
# Terms and conditions:
# By using this script you are legally obliged to buy the author a beer or a mate.
# Please deliver to office 12/15 @ TTP.
# Not complying will result in terrible segmetation faults and
# numpy compatiblitity issues in all of your projects.
#
# Usage:
# Put the following into the file ~/.condadirs
# 
# /home/user/exampledir : myenv
# 
# in order to automatically activate the conda enviroment myenv in
# the directory "/home/user/exampledir" and its subdirectories.
#
# In your .zshrc add the following:
# source $HOME/.local/bin/condadir.sh


if [ ! -f "$HOME/.condadirs" ]; then
    touch .condadirs
fi

declare -a cdir=()
declare -a cenv=()
OLD_IFS=$IFS
IFS=$'\n'
for line in $(cat $HOME/.condadirs); do
    # separate by :
    envdir=$(sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"${line}")
    envdir=("${(@s/:/)line}")
    dir=$(sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"$envdir[1]")
    env=$(sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"$envdir[2]")
    cenv+=($env)
    cdir+=($dir)
done

IFS=$OLD_IFS

# now sort them by length!
indices=( $(for ((i = 1; i <= ${#cdir[@]}; i++)); do
    printf '%s %s %s\n' $i "${#cdir[i]}" "${cdir[i]}"
      done | sort -nk2,2 -k3 | cut -f1 -d' '))

declare -a _CDIRS=()
declare -a _CENVS=()

for i in $indices; do
    _CDIRS+=$cdir[$i]
    _CENVS+=$cenv[$i]
done

ACTIVE_CONDA_ENV=""
for (( j=1; j<=${#_CDIRS[@]}; j++ ));do
    if [[ $(pwd) == "$_CDIRS[$j]"* ]]; then	
	ACTIVE_CONDA_ENV=$_CENVS[$j]	
    fi
done
    
if [[ "$ACTIVE_CONDA_ENV" != "" ]]; then
    conda activate $ACTIVE_CONDA_ENV
fi

function conda_cd () {
    # when chaning into a specified directory activate the conda enviroment
    \cd "$@" &&
	activate_env=""
	for (( j=1; j<=${#_CDIRS[@]}; j++ ));do	    
	    if [[ $(pwd) == "$_CDIRS[$j]"* ]]; then
		activate_env=$_CENVS[$j]
	    fi	 
	done
	    if [[ $activate_env != $ACTIVE_CONDA_ENV ]] && [[ $activate_env != "" ]]; then
		if [[ $ACTIVE_CONDA_ENV != "" ]]; then
		    conda deactivate
		fi
	    conda activate $activate_env
	    ACTIVE_CONDA_ENV=$activate_env
	elif [[ $activate_env == "" ]] && [[ $CONDA_SHLVL -ne 0 ]]; then	    
	    conda deactivate
	    ACTIVE_CONDA_ENV=""
	fi       
}

alias cd="conda_cd"



