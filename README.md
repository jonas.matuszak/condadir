# Condadir

Available for ZSH and BASH.

## Installation:

Please backup your `.bashrc` or `.zshrc` configuration:

`cp .zshrc .zshrc_backup`

`cp .bashrc .bashrc_backup`


### Automatic installation:

For zsh: 

`zsh -c "$(curl -fsSL https://git-ce.rwth-aachen.de/jonas.matuszak/condadir/-/raw/main/install-condadir.sh)"`


For bash:

`sh -c "$(curl -fsSL https://git-ce.rwth-aachen.de/jonas.matuszak/condadir/-/raw/main/install-condadir.sh)"`

### Manual installation:

Copy the file  `condadir.sh` or `condadir.zsh` to your favorit location and add 

`source /yourlocation/condadir.sh` 

`source /yourlocation/condadir.zsh`

to your `.bashrc` or `.zshrc` file.


## Configuration

Create a file `.condadirs` in your home folder and add directories in which you want 
conda to activate an enviroment automatically as:

`/your/automatic/conda/env/dir : your_env`

Then read the license agreement carefully.
