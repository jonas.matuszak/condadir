
THIS_SHELL="$(ps -hp $$ | awk '{print $5}')"

mkdir -p $HOME/.local/bin

if [[ $THIS_SHELL == "/bin/zsh" ]] || [[ $THIS_SHELL == "zsh"  ]]; then
    echo "Installing condadir for the ZSH shell:\n"
    echo "Backing up .zshrc into .zshrc_pre_condadir\n"
    cp $HOME/.zshrc $HOME/.zshrc_pre_condadir
    echo "Downloading condadir\n"    
    wget https://git-ce.rwth-aachen.de/jonas.matuszak/condadir/-/raw/main/condadir.zsh -O $HOME/.local/bin/condadir.zsh
    echo "Adding condadir to .zshrc\n"
    echo "\n# condadir.zsh configuration
# automatically added by installer
source $HOME/.local/bin/condadir.zsh" >> $HOME/.zshrc
    echo "\n"
    echo "Sucessfully installed condadir!"
elif [[ $THIS_SHELL == "bash" ]] || [[ $THIS_SHELL == "/bin/bash" ]]; then
    echo -e "Installing condadir for the BASH shell:\n"
    echo -e "Backing up .bashrc into .bashrc_pre_condadir\n"
    cp $HOME/.bashrc $HOME/.bashrc_pre_condadir
    wget https://git-ce.rwth-aachen.de/jonas.matuszak/condadir/-/raw/main/condadir.sh -O $HOME/.local/bin/condadir.sh
    echo -e "Adding condadir to .bashrc\n"
    echo -e "\n# condadir.sh configuration
# automatically added by installer
source $HOME/.local/bin/condadir.sh" >> $HOME/.bashrc
    echo -e "\n"
    echo "Sucessfully installed condadir."
else
    echo "Shell is not BASH or ZSH"
    echo "Abort installation"
fi
